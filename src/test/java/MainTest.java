import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;
import java.util.stream.Stream;


public class MainTest {

    public static final Logger LOGGER = LogManager.getLogger(MainTest.class.getName());

    @ParameterizedTest
    @MethodSource("createParameters")
    public void assureSequence(Argument argument) {
        List<Integer> list = argument.getList();
        int longestSequencePosition = -1;
        int longestSequenceLength = 0;
        for (int i = 1; i < list.size() - 1; i++) {
            if (list.get(i) == list.get(i + 1)) {
                int sequentionValue = list.get(i);
                if (list.get(i - 1) < sequentionValue) {
                    for (int j = i; list.get(j) == sequentionValue; j++) {
                        if (list.get(j + 1) < sequentionValue) {
                            //Success!
                            int sequenceLength = j - i + 1;
                            if (sequenceLength > longestSequenceLength) {
                                longestSequencePosition = i;
                                longestSequenceLength = sequenceLength;
                            }
                            i = j + 1;
                        } else if (list.get(j + 1) > sequentionValue) {
                            //Fail
                            i = j + 1;
                            break;
                        } //else continue
                    }
                }
            }
        }
        assertEquals(longestSequenceLength, argument.getExpectedLength());
        assertEquals(longestSequencePosition, argument.getExpectedPosition());
    }

    public static Stream<Argument> createParameters() {
        Set<Argument> arguments = new HashSet<>();
        arguments.add(new Argument(
                Arrays.asList(new Integer[]{1, 4, 4, 4, 2}), 3,1
        ));

        arguments.add(new Argument(
                Arrays.asList(new Integer[]{1,2,2,3,4,4,4,3,3,3,1}), 3,4
        ));

        arguments.add(new Argument(
                Arrays.asList(new Integer[]{1}), 0,-1
        ));

        arguments.add(new Argument(
                Arrays.asList(new Integer[]{1,1,1,1,1,1}), 0,-1
        ));


        return arguments.stream();
    }
}

class Argument {
    private List<Integer> list;
    private int expectedLength;
    private int expectedPosition;

    public Argument(List<Integer> list, int expectedLength, int expectedPosition) {
        this.list = list;
        this.expectedLength = expectedLength;
        this.expectedPosition = expectedPosition;
    }

    public List<Integer> getList() {
        return list;
    }

    public int getExpectedLength() {
        return expectedLength;
    }

    public int getExpectedPosition() {
        return expectedPosition;
    }
}
