import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.IntStream;



public class Main {

    public static final Logger LOGGER = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        List<Integer> list;
        LOGGER.info("enter your ints sequence, or enter any key to abort");
        list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            list.add(scanner.nextInt());
        }
        if (list.isEmpty()) {
            LOGGER.error("no ints entered; aborting further execution");
            return;
        }
        int longestSequencePosition = -1;
        int longestSequenceLength = 0;
        for (int i = 1; i < list.size() - 1; i++) {
            if (list.get(i) == list.get(i + 1)) {
                int sequentionValue = list.get(i);
                if (list.get(i - 1) < sequentionValue) {
                    for (int j = i; list.get(j) == sequentionValue; j++) {
                        if (list.get(j + 1) < sequentionValue) {
                            //Success!
                            int sequenceLength = j - i + 1;
                            if (sequenceLength > longestSequenceLength) {
                                longestSequencePosition = i;
                                longestSequenceLength = sequenceLength;
                            }
                            i = j + 1;
                        } else if (list.get(j + 1) > sequentionValue) {
                            //Fail
                            i = j + 1;
                            break;
                        } //else continue
                    }
                }
            }
        }
        if (longestSequencePosition == -1) {
            LOGGER.info("no specified sequences found; perhaps there are any?");
        } else {
            LOGGER.info(String.format("Longest sequention starts at %d and maintains %d elements",
                    longestSequencePosition,
                    longestSequenceLength));
        }
    }
}
